# Simile-TimeLine_4_Nextcloud

Ich möchte gerne Simile TimeLine innerhalb von Nextcloud nutzen. 
Simile TimeLine ist vorhanden, Nextcloud ebenso. 

Wie können die beiden sinnvoll zusammengebracht werden?

http://simile.mit.edu/timeline/

https://nextcloud.com

## Brainstorming:

Identifizieren welcher Simile-Timeline Code ohne Referenzen zu bestimmten dritten Servern auskommt. 
    (Alle benötigten Dateien müssen lokal liegen)

Grundkonzept von Nextcloud-Apps finden und verstehen

    * Ich denke das sinnvollste ist es, eine Nextcloud-App draus zu machen.
    * Daten in die Nextcloud-Datenbank? In mehrere xml-Files?
    * Frontend als eigenen "Menüpunkt" wie Bookmarks oder Notes.

Features:

    * Evtl. Kalender-Daten in XML-File übernehmen. 
        ** Kompletten Kalender oder
        ** Einzelne Termin-Einträge
            ***Mit oder ohne dauerhafte Markierung/Verbindung zu den Timeline-Daten
